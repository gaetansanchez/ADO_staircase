% Example to use the ADO (Adaptive Design Optimization) staircase
%----------------------------------------------------------------
% The main function ADO_staircase.m needs a specific trial function 
% as input in fun_run_trial
% [cfg,correct] = fun_run_trial(cfg);
% your fun_run_trial should be able to:
% - accept a structure 'cfg' (after the staircase the final stim intensity will be store in cfg.thresh) 
% - provide an output 'correct' 1 or 0 according to participant answer
% 'correct/detected' --> correct = 1 
% or 
% 'incorrect/undetected' --> correct = 0
%
% However one could call the function with fun_run_trial = []
% This will lead to run the function using a simulated subject with a
% specific sigmoid in the range of gridu provided

%% Sigmoid fit with adaptive design
%-----------------------------------
cfg           = [];
cfg.classicdesign.doit = 0;

fun_run_trial = []; % or @Mytrialfunction

gridu         = linspace(0, 3, 500);
%gridu         = linspace(0, 100, 500);

if cfg.classicdesign.doit
  section = 4; % steps of gridu for random selection during initialization
  vec_sel = [0:section].*(max(gridu)/section);
  cfg.classicdesign.gridu = vec_sel;
end

nbtrialmax    = 50;
th            = .5;
flagplot      = 1;

[thresh, sc] = ADO_staircase(cfg,gridu,nbtrialmax,fun_run_trial,th,flagplot);

%% Plot final results (predicted sigmoid vs. real simulated sigmoid)
%-------------------------------------------------------------------
figure;set(gcf,'color','w');

in.G0   = 1;
in.S0   = 0;
in.beta = 1;
in.INV  = 0;
Phi     = sc.muPhi;
simphi  = sc.ADO.simphi;
Priophi = sc.ADO.options.priors.muPhi;
y = sigm(gridu,in,Phi);
plot(gridu,y, 'k-','LineWidth',3); hold on;
y = sigm(gridu,in,simphi);
plot(gridu,y, 'r--','LineWidth',3);
y = sigm(gridu,in,Priophi);
plot(gridu,y, 'g','LineWidth',2);
title('Sigmoid function fitted');
xlabel('Stimulus value (e.g. intensity)','Fontsize',15,'Fontweight','bold');
ylabel('Probability of stimulus detection','Fontsize',15,'Fontweight','bold');
legend({'predicted sigmoid',...
    'real sigmoid (simulation)','prior sigmoid'},'Location','Best','Fontsize',15,'Fontweight','bold');
set(gca,'Fontsize',15);
  
  